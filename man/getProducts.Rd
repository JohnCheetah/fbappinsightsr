% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/get_products.R
\name{getProducts}
\alias{getProducts}
\title{Get purchaseable products}
\usage{
getProducts(id, token)
}
\arguments{
\item{id}{Application id.}

\item{token}{A valid token as returned by \code{\link{fbAuthenticate}}
 or a short-term token from
\href{https://developers.facebook.com/tools/explorer}{facebook Graph API Explorer}.}
}
\description{
Get in-app-purchaseable products associated with this app
}
\examples{
\dontrun{
TK <- fbAuthenticate(app.id = "1234567890123456",
                     app.secret = "16xx79321xx0130x2x10a08x3e2x80xx")
id <- sample(getApps(token)$id, 1) # sample app id
products <- getProducts(id, TK)
}

}
\author{
John Coene \email{john.coene@cmcm.com}
}

