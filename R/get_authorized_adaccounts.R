#' Get authorized accounts
#'
#' @description Get ad accounts which are authorized to advertise for this app
#'
#' @inheritParams getAgencies
#' @param n Number of results to return, defaults to \code{25}
#'
#' @examples
#' \dontrun{
#' TK <- fbAuthenticate(app.id = "1234567890123456",
#'                      app.secret = "16xx79321xx0130x2x10a08x3e2x80xx")
#' id <- sample(getApps(token)$id, 1) # sample app id
#' accounts <- getAdAccounts(id, TK)
#' }
#'
#' @export
#'
#' @author John Coene \email{john.coene@@cmcm.com}
getAdAccounts <- function(id, token, n = 25){
  if (missing(id)) {
    stop("Missing id")
  }
  if (missing(token)) {
    stop("Missing token")
  }
  uri <- paste0("https://graph.facebook.com/v2.6/", id, "/authorized_adaccounts")
  resp <- httr::GET(url = uri, query = list(access_token = token))
  httr::stop_for_status(resp)
  content <- httr::content(resp)
  dat <- do.call(plyr::"rbind.fill", lapply(content$data, as.data.frame))
  while(n > nrow(dat)  && length(content$paging$`next`)){
    resp <- httr::GET(content$paging$`next`)
    content <- httr::content(resp)
    page <- do.call(plyr::"rbind.fill", lapply(content$data, as.data.frame))
    dat <- plyr::rbind.fill(dat, page)
  }
  return(dat)
}
