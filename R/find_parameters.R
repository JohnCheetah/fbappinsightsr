#' Find parameters
#'
#' @author John Coene \email{john.coene@@cmcm.com}
#' @name find
NULL

#' @rdname find
#' @export
findKeyMetrics <- function(){
  return(sort(keymetrics))
}

#' @rdname find
#' @export
findBreakdowns <- function(){
  return(sort(breakdown))
}

#' @rdname find
#' @export
findAggregateBy <- function(){
  return(sort(agg_by))
}
