#' Facebook analytics for apps
#'
#' R interface to Facebook analytics for apps
#'
#' @section functions:
#' \itemize{
#'    \item \code{\link{fbAuthenticate}}
#'    \item \code{\link{getApps}}
#'    \item \code{\link{getAchievements}}
#'    \item \code{\link{getAgencies}}
#'    \item \code{\link{getAppInfo}}
#'    \item \code{\link{getAppLinks}}
#'    \item \code{\link{getAdAccounts}}
#'    \item \code{\link{getCustomAudience}}
#'    \item \code{\link{getAndroidDialogs}}
#'    \item \code{\link{getiOSDialogs}}
#'    \item \code{\link{getEventInsights}}
#'    \item \code{\link{getEventTypes}}
#'    \item \code{\link{getEvents}}
#'    \item \code{\link{getInsights}}
#'    \item \code{\link{getEventInsights}}
#'    \item \code{\link{getPicture}}
#'    \item \code{\link{getProducts}}
#' }
#'
#' @examples
#' \dontrun{
#' # Authenticate
#' TK <- fbAuthenticate(app.id = "1234567890123456",
#'                      app.secret = "16xx79321xx0130x2x10a08x3e2x80xx")
#'
#' # find your apps
#' apps <- getApps(token)
#' }
#'
#' @author John Coene \email{john.coene@@cmcm.com}
#'
#' @docType package
#' @name fbAppInsightsR
#'
#' @importFrom utils packageVersion URLencode
NULL
