#' Get App Link hosts
#'
#' @description Get app Link hosts
#'
#' @inheritParams getAgencies
#'
#' @examples
#' \dontrun{
#' TK <- fbAuthenticate(app.id = "1234567890123456",
#'                      app.secret = "16xx79321xx0130x2x10a08x3e2x80xx")
#' id <- sample(getApps(token)$id, 1) # sample app id
#' links <- getAppLinks(id, TK)
#' }
#'
#' @export
#'
#' @author John Coene \email{john.coene@@cmcm.com}
getAppLinks <- function(id, token){
  if (missing(id)) {
    stop("Missing id")
  }
  if (missing(token)) {
    stop("Missing token")
  }
  uri <- paste0("https://graph.facebook.com/v2.6/", id, "/app_link_hosts")
  resp <- httr::GET(url = uri, query = list(access_token = token))
  httr::stop_for_status(resp)
  content <- httr::content(resp)
  content <- do.call(plyr::"rbind.fill", lapply(content$data, as.data.frame))
  return(content)
}
