
![Developped by Cheetah Mobile](https://upload.wikimedia.org/wikipedia/en/f/f9/Cheetah_Mobile_Logo.png)

fbAppsInsightsR
===============

R interface to Facebook analytics for apps

Install
-------

``` r
devtools::install_bitbucket("JohnCheetah/fbadsinsightsr", auth_user = "your_login", password = "your_password")
```

Functions
---------

-   `fbAuthenticate`
-   `getApps`
-   `getAchievements`
-   `getAgencies`
-   `getAppInfo`
-   `getAppLinks`
-   `getAdAccounts`
-   `getCustomAudience`
-   `getAndroidDialogs`
-   `getiOSDialogs`
-   `getEventInsights`
-   `getEventTypes`
-   `getEvents`
-   `getInsights`
-   `getEventInsights`
-   `getPicture`
-   `getProducts`

Versions and Patches
--------------------

See NEWS.md for changes
